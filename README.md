# fuchsia-build [![Fuchsia logo](docs/img/logos/fuchsia.png)](https://fuchsia.googlesource.com/docs/+/refs/heads/master/README.md) [![Docker logo](docs/img/logos/docker.png)](https://www.docker.com/)
> A docker image to easily build Google's Fuchsia OS.

This is still work in progress.  
A detailed README will be added later on.
